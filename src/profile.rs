#![allow(dead_code)]
extern crate regex;

use std::collections::btree_map::BTreeMap;
use std::collections::HashMap;
// use self::regex::Regex;

pub fn edit_distance(string1: &Vec<u8>, string2: &Vec<u8>) -> u8{
    let mut distance = 0;

    for (index, value1) in string1.iter().enumerate() {
        let value2 = string2[index];
        let bin1 = format!("{:>08b}", value1);
        let bin2 = format!("{:>08b}", value2);

        // println!("--\n{} {}\n{} {}", bin1, value1, bin2, value2);

        for (i, char1) in bin1.chars().enumerate() {
            match bin2.chars().nth(i) {
                Some(c) => {
                    if char1 != c {
                        distance = distance + 1;
                    }
                },
                None => {}
            };
        }

    }
    distance
}

pub fn count_characters(text: &str) -> BTreeMap<char, f64> {
    let mut count = BTreeMap::new();

    for c in text.chars() {
        *count.entry(char::to_uppercase(c).next().unwrap()).or_insert(0.0) += 1.0;
    }
    // println!("{:?}", count);
    count
}

pub fn relative_characters(text: &str) -> BTreeMap<char, f64> {
    let length: f64 = text.len() as f64;
    let mut count = count_characters(text);

    for val in count.values_mut() {
        *val = *val / length;
    }
    // println!("{:?}\n--\n", count);

    count
}

pub fn score_text(text: &str, english_frequencies: &HashMap<char, f64>) -> f64 {
    let distribution = relative_characters(text);

    let mut score = 0.0;

    for (character, value) in distribution {
        // let string_char = String::from_
        let expected_val = english_frequencies.get(&character);
        match expected_val {
            Some(x) => {
                let product = x * value;
                // println!("{}\n{} expected\n{} seen\n\ninc {} sqrt {}\n\n", character, x, value, product, product.sqrt());
                score = score + product.sqrt();
            },
            _ => {},
        }

    }
    score = match score {
        0.0 => 2.0,
        _ => -score.ln()
    };
    score
}

pub fn deterministic_frequencies(buffer: &Vec<u8>) -> BTreeMap<u8, f64> {
    let length: f64 = buffer.len() as f64;

    let mut count = BTreeMap::new();

    for c in buffer.iter() {
       *count.entry(*c).or_insert(0.0) += 1.0;
    }

    for val in count.values_mut() {
        *val = *val / length;
    }
    // println!("{:?}\n--\n", count);

    count
}


pub fn detect_ecb(hex_string: String, block_size: usize) -> Option<String> {
    let mut detected = None;
    let hex_length = block_size * 2; // 2 hex chars for 1 byte

    // let mut processing: &str = &hex_string;
    let mut to_process = hex_string.len();
    let mut block_num = 0;
    while to_process > hex_length {
        let offset = hex_length * block_num;
        let end_offset = offset + hex_length;
        let block = &hex_string[offset..end_offset];
        let rest = &hex_string[end_offset..];
        to_process = rest.len();
        block_num = block_num + 1;
        // println!("{} {}", block, to_process);
        if rest.contains(block) {
            println!("ECB mode detected! Repeated block: {}", block);
            detected = Some(hex_string.to_owned());
            break;
        }
    }

    detected
}

pub fn english_frequencies() -> HashMap<char, f64> {
    let mut english_frequencies = HashMap::new();
    english_frequencies.insert("E".to_string().chars().next().unwrap(), 0.1202 as f64);
    english_frequencies.insert("T".to_string().chars().next().unwrap(), 0.0910 as f64);
    english_frequencies.insert("A".to_string().chars().next().unwrap(), 0.0812 as f64);
    english_frequencies.insert("O".to_string().chars().next().unwrap(), 0.0768 as f64);
    english_frequencies.insert("I".to_string().chars().next().unwrap(), 0.0731 as f64);
    english_frequencies.insert("N".to_string().chars().next().unwrap(), 0.0695 as f64);
    english_frequencies.insert("S".to_string().chars().next().unwrap(), 0.0628 as f64);
    english_frequencies.insert("R".to_string().chars().next().unwrap(), 0.0602 as f64);
    english_frequencies.insert("H".to_string().chars().next().unwrap(), 0.0592 as f64);
    english_frequencies.insert("D".to_string().chars().next().unwrap(), 0.0432 as f64);
    english_frequencies.insert("L".to_string().chars().next().unwrap(), 0.0398 as f64);
    english_frequencies.insert("U".to_string().chars().next().unwrap(), 0.0288 as f64);
    english_frequencies.insert("C".to_string().chars().next().unwrap(), 0.0271 as f64);
    english_frequencies.insert("M".to_string().chars().next().unwrap(), 0.0261 as f64);
    english_frequencies.insert("F".to_string().chars().next().unwrap(), 0.0230 as f64);
    english_frequencies.insert("Y".to_string().chars().next().unwrap(), 0.0211 as f64);
    english_frequencies.insert("W".to_string().chars().next().unwrap(), 0.0209 as f64);
    english_frequencies.insert("G".to_string().chars().next().unwrap(), 0.0203 as f64);
    english_frequencies.insert("P".to_string().chars().next().unwrap(), 0.0182 as f64);
    english_frequencies.insert("B".to_string().chars().next().unwrap(), 0.0149 as f64);
    english_frequencies.insert("V".to_string().chars().next().unwrap(), 0.0111 as f64);
    english_frequencies.insert("K".to_string().chars().next().unwrap(), 0.0069 as f64);
    english_frequencies.insert("X".to_string().chars().next().unwrap(), 0.0017 as f64);
    english_frequencies.insert("Q".to_string().chars().next().unwrap(), 0.0011 as f64);
    english_frequencies.insert("J".to_string().chars().next().unwrap(), 0.0010 as f64);
    english_frequencies.insert("Z".to_string().chars().next().unwrap(), 0.0007 as f64);
    english_frequencies
}
