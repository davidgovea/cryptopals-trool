// extern crate openssl;
// extern crate rand;
extern crate rustc_serialize;
extern crate url;
extern crate regex;

use rustc_serialize::hex::{FromHex, ToHex};
use rustc_serialize::base64::{self,ToBase64};
use std::collections::HashMap;
use regex::Regex;

#[path = "../crypto.rs"]
mod crypto;
#[path = "../buffer.rs"]
mod buffer;
#[path = "../profile.rs"]
mod profile;

// For this challenge, we need 2 pre-fab blocks from the oracle:
//  * One that ends in "role="
//  * One that starts with "admin"

fn main() {
    println!("-- Set 2, Challenge 13 --");

    let query_string = "foo=bar&baz=qux&zap=zazzle";
    let hash_query = parse_profile(&query_string);
    assert_eq!(hash_query.get("foo").unwrap(), "bar");
    println!("Parsed profile {:?}", hash_query);

    // println!("{:?}", crypto::rand_aes_key().to_hex());
    let key = "d27ee4c03d02a57f38d208cdd30dff68".from_hex().unwrap();

    let chosen_inputs = [
        "govea.d@gmail.com",
        "oneblock1",
        "twoblocks2",
        "1blockaf",
        "adminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadmin",
        "admin.d@gmail.com",
        "1dmin2dmin3dmin4dmin5dmin6dmin7dmin8dminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadminadmin",
        "__________AAAAAAAAAAAAAAAA",
        "BBBBBBBBBB",
        "BBBBBBBBBBuser",
        "BBBBBBBBBBuse",
        "oneblock1",
        "at@tak.co",
        "__________XXX",
        "__________admin           ",
        "__________admin===========",

        // "uid_10",
    ];
    println!("\n=== Oracle output to attacker ===\n");
    for email in chosen_inputs.iter() {
        let profile = profile_for(email);
        let encrypted_profile = crypto::ecb_encrypt(&key, &profile.to_string().into_bytes());  
        println!("{}\n{}\n\n", &profile, &encrypted_profile.to_hex());
    }

    let encrypted_profile_inputs = [
        // // oneblock1 hash, unmodified
        // "4edf5a5791d4d5b4116bfad9366418a55c4bd65a5c47812c41e051de7a3cd02a182f65fb27882fe9291179a583514bba",
        // // oneblock1 hash with first block replaced
        // "124a19d4664ae3bf81e32ad5b2f9fc1f5c4bd65a5c47812c41e051de7a3cd02a182f65fb27882fe9291179a583514bba",
        // // oneblock1 with second block replaced
        // "4edf5a5791d4d5b4116bfad9366418a5124a19d4664ae3bf81e32ad5b2f9fc1f182f65fb27882fe9291179a583514bba",

        // "4edf5a5791d4d5b4116bfad9366418a5d7a9d508887d80ad510469e2552c6247182f65fb27882fe9291179a583514bba",
        // "4edf5a5791d4d5b4116bfad9366418a52e32be88c62c4a5f34f2b84b29bd1c72182f65fb27882fe9291179a583514bba",
        // "4edf5a5791d4d5b4116bfad9366418a577152744137f8b7f484b62b275a7bba1a4a0b0ced798e88ba940146b6f601b2a182f65fb27882fe9291179a583514bba",
        // "4edf5a5791d4d5b4116bfad9366418a577152744137f8b7f484b62b275a7bba15c24f8ff4cfa63732ce71b58e002204f182f65fb27882fe9291179a583514bba",
        // "4edf5a5791d4d5b4116bfad9366418a55c24f8ff4cfa63732ce71b58e002204f182f65fb27882fe9291179a583514bba",
        // "4edf5a5791d4d5b4116bfad9366418a5c153e04b963d9ebf02032b16c06562b2c37a37cba1fac6220d835383c31d87e2182f65fb27882fe9291179a583514bba",
        "6171a88eaf247997058ddafa48111c8fc153e04b963d9ebf02032b16c06562b2ef977693dfcd7eb4682e6bb7e638150c182f65fb27882fe9291179a583514bba",

    ];

    println!("\n=== Parsed attacker inputs ===\n");
    for input in encrypted_profile_inputs.iter() {
        println!("input: {}", input);
        let decrypted = crypto::ecb_decrypt(&key, &input.from_hex().unwrap());
        let decrypted_profile = buffer::stringify(&decrypted);
        let parsed_profile = parse_profile(&decrypted_profile);
        // println!("{}\n{:?}\n\n", &decrypted_profile, &parsed_profile);
        assert_eq!(parsed_profile.get("role").unwrap(), "admin");
        println!("ADMIN ROLE ACHIEVED");
    }

}

fn parse_profile(input: &str) -> HashMap<String, String> {
    url::form_urlencoded::parse(input.as_bytes())
        .fold(HashMap::new(), |mut hash, (k, v)| {
            hash.insert(k.to_string(), v.to_string());
            hash
        })
}

fn profile_for(email: &str) -> String {
    let control_regex = Regex::new(r"[&=]").unwrap();
    format!("email={}&uid=10&role=user", control_regex.replace_all(email, ""))
}
