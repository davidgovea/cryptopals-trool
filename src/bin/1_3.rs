extern crate regex;
extern crate rustc_serialize;

use std::str;
// use regex::Regex;
use rustc_serialize::hex::FromHex;

#[path = "../buffer.rs"]
mod buffer;

#[path = "../profile.rs"]
mod profile;

fn main() {
    let ciphertext = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";

    let cipherdata = ciphertext.from_hex().unwrap();
    let english_frequencies = profile::english_frequencies();

    println!("-- Set 1, Challenge 3 --");

    for i in 0..254 {
        let result = buffer::xor_vec(&cipherdata, vec![i]);
        // let re = Regex::new(r"^[a-zA-Z\s',:;?!\(\)$]+$").unwrap();
        // if re.is_match(buffer::stringify(&result)) {
        //     println!("Potential match with key: {}", i);
        //     println!("{}", buffer::stringify(&result));
        // }
        let score = profile::score_text(buffer::stringify(&result), &english_frequencies);
        println!("{} score: {}\n{}\n\n", i, score, buffer::stringify(&result));
    }

    // println!("{}", plaintext);
}

// Cooking MC's like a pound of bacon
