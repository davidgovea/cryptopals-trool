extern crate openssl;
extern crate rustc_serialize;
extern crate regex;

use std::io::prelude::*;
use std::fs::File;
use std::io::BufReader;
use rustc_serialize::base64::FromBase64;
use rustc_serialize::hex::FromHex;
// use openssl::crypto::symm::{decrypt, Type};

#[path = "../buffer.rs"]
mod buffer;

#[path = "../profile.rs"]
mod profile;

fn main() {
    println!("-- Set 1, Challenge 8 --");

    let eng = profile::english_frequencies();
    let mut eng_sorted: Vec<_> = eng.iter().collect();
    eng_sorted.sort_by(|a, b| b.1.partial_cmp(a.1).unwrap());

    let mut f = File::open("8.txt").unwrap();

    let file = BufReader::new(&f);
    for (index, possible_ecb_cipher) in file.lines().enumerate() {
        let cipher = possible_ecb_cipher.unwrap();
        let cipherdata = cipher.from_hex().unwrap();

        let ecb_cipher = profile::detect_ecb(cipher, 16);
        match ecb_cipher {
            Some(v) => println!("ECB detected on ciphertext: {}", v),
            None => {}
        }

    }

}

// -- Set 1, Challenge 8 --
// Found duplicate block 08649af70dc06f4fd5d2d69c744cd283
// Ciphertext:
// d880619740a8a19b7840a8a31c810a3d08649af70dc06f4fd5d2d69c744cd283e2dd052f6b641dbf9d11b0348542bb5708649af70dc06f4fd5d2d69c744cd2839475c9dfdbc1d46597949d9c7e82bf5a08649af70dc06f4fd5d2d69c744cd28397a93eab8d6aecd566489154789a6b0308649af70dc06f4fd5d2d69c744cd283d403180c98c8f6db1f2a3f9c4040deb0ab51b29933f2c123c58386b06fba186a
