extern crate openssl;
extern crate rand;
extern crate rustc_serialize;

use std::io::prelude::*;
use std::fs::File;
use rustc_serialize::base64::FromBase64;
use openssl::symm::{decrypt, encrypt, Cipher};
use rand::Rng;
use rustc_serialize::hex::ToHex;
use rustc_serialize::hex::FromHex;
use std::str;

#[path = "../crypto.rs"]
mod crypto;
#[path = "../buffer.rs"]
mod buffer;
#[path = "../profile.rs"]
mod profile;

fn main() {
    println!("-- Set 2, Challenge 12 --");


    let initial_ciphertext_length = black_box2(&"A").len();
    let mut block_size = 0;
    let mut current_input_length = 2;

    while block_size == 0 {
        let input = vec!["A"; current_input_length].join("");

        let new_ciphertext_length = black_box2(&input).len();

        match new_ciphertext_length > initial_ciphertext_length {
            true => {
                block_size = new_ciphertext_length - initial_ciphertext_length;
            },
            false => {
                current_input_length += 1;
            }
        }
    }
    println!("Discovered block size: {}", block_size);

    let repeater_payload = vec!["A"; block_size * 3].join("");
    println!("Checking ECB use with 3-block payload {}", repeater_payload);
    profile::detect_ecb(black_box2(&repeater_payload).to_hex(), block_size);

    let mut guessed_bytes: Vec<u8> = vec![];

    for block_index in 0..initial_ciphertext_length/block_size {
        let starting_index = block_index * block_size;
        let block_end_index = starting_index + block_size;
        // println!("start_index: {}, block_end_index: {}", &starting_index, &block_end_index);

        for block_position_reverse in 0..block_size {
            let block_position = block_size - 1 - block_position_reverse;

            let repeated_prefix = vec!["A"; block_position].join("");
            // let oracle_payload = format!("{}{}", buffer::stringify(&guessed_bytes[0..starting_index].to_owned()), repeated_prefix);
            let oracle_payload = &repeated_prefix;

            let reference_block = black_box2(&oracle_payload)[starting_index..block_end_index].to_owned();
            // println!("capturing block {}: {}\n{:?}\n\n", &block_index, &oracle_payload, reference_block);
            // println!("ref block: {} {:?} {:?}", &oracle_payload, guessed_bytes, reference_block);
            for byte in 0..128 {

                let brute_byte = vec![byte as u8];
                let brute_char = buffer::stringify(&brute_byte);
                let mut padding = vec![];
                let mut last_guesses: Vec<u8> = guessed_bytes.iter().cloned().collect();

                match guessed_bytes.len() >= block_size {
                    true => {
                        last_guesses = guessed_bytes.iter().skip(starting_index - block_position).cloned().collect();
                    },
                    false => {
                        padding.resize(block_size - guessed_bytes.len() - 1, "A");
                    }
                }

                let brute_payload = format!("{}{}{}", padding.join(""), buffer::stringify(&last_guesses), brute_char);

                let brute_block = black_box2(&brute_payload)[0..16].to_owned();
                // println!("byte {}, payload {}\n{:?}", byte, brute_payload, brute_block);

                if brute_block == reference_block {
                    // println!("FOUND MATCH: {}\n{:?}\n{:?}", &brute_payload, &reference_block, &brute_block);
                    guessed_bytes.push(byte as u8);
                    // println!("Cracked byte: {}", byte);
                    break;
                }
            }
        }
    }

    println!("{}", buffer::stringify(&guessed_bytes));
}

fn black_box2(input: &str) -> Vec<u8> {
    let key = "ba1cd0cd7db8db95e065dba17379a053".to_string().from_hex().unwrap();
    let unknown_string = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK".from_base64().unwrap();
    let mut input_bytes = input.to_string().into_bytes();
    input_bytes.extend(unknown_string);

    crypto::ecb_encrypt(&key, &input_bytes)
}
