extern crate regex;
extern crate rustc_serialize;

use std::str;
use rustc_serialize::hex::ToHex;

#[path = "../buffer.rs"]
mod buffer;

fn main() {
    println!("-- Set 1, Challenge 5 --");
    let plaintext = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
    let data = plaintext.to_string().into_bytes();
    let key = "ICE".to_string().into_bytes();

    println!("{}", plaintext);
    // println!("{:?}", data);

    let cipherdata = buffer::xor_vec(&data, key);
    println!("{}", cipherdata.to_hex());

    // // decrypt
    // key = "ICE".to_string().into_bytes();
    // println!("{}", stringify(&buffer::xor_vec(&cipherdata, key)));

}

// -- Set 1, Challenge 5 --
// Burning 'em, if you ain't quick and nimble
// I go crazy when I hear a cymbal
// 0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f
