extern crate openssl;
extern crate rustc_serialize;

use std::io::prelude::*;
use std::fs::File;
use rustc_serialize::base64::FromBase64;
use openssl::symm::{decrypt, encrypt, Cipher};

#[path = "../buffer.rs"]
mod buffer;

#[path = "../crypto.rs"]
mod crypto;


fn main() {
    println!("-- Set 1, Challenge 7 --");

    let mut f = File::open("7.txt").unwrap();
    let mut ciphertext = String::new();
    f.read_to_string(&mut ciphertext).unwrap();

    let cipherdata = ciphertext.from_base64().unwrap();
    let key = "YELLOW SUBMARINE".to_string().into_bytes();

    let plain = crypto::ecb_decrypt(&key, &cipherdata);
    let reenc = crypto::ecb_encrypt(&key, &plain);
    let plain2 = crypto::ecb_decrypt(&key, &reenc);

    println!("{}", buffer::stringify(&plain2));
}

// Same message as 1.6
