extern crate rustc_serialize;

use rustc_serialize::base64::{self, ToBase64};
use rustc_serialize::hex::FromHex;

fn main() {
    let input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    let data = input.from_hex();
    // Need to learn more about unwrap();
    let unwrapped = data.unwrap();
    let base64 = unwrapped.to_base64(base64::STANDARD);

    println!("-- Set 1, Challenge 1 --");
    println!("hex input: {}", input);
    println!("{}", base64);
}


// http://stackoverflow.com/questions/26185485/how-to-convert-hexadecimal-values-to-base64-in-rustlang
