extern crate openssl;
extern crate rustc_serialize;

use std::io::prelude::*;
use std::fs::File;
use rustc_serialize::base64::FromBase64;
use openssl::symm::{decrypt, encrypt, Cipher};

#[path = "../crypto.rs"]
mod crypto;
#[path = "../buffer.rs"]
mod buffer;

fn main() {
    println!("-- Set 2, Challenge 10 --");

    let mut f = File::open("10.txt").unwrap();
    let mut ciphertext = String::new();
    f.read_to_string(&mut ciphertext).unwrap();

    let cipherdata = ciphertext.from_base64().unwrap();
    let key = "YELLOW SUBMARINE".to_string().into_bytes();

    let block_size: usize = 16;
    let iv = vec![0 ; 16];

    let decrypted = crypto::cbc_decrypt(&cipherdata, &key, &iv, block_size);
    let reenc = crypto::cbc_encrypt(&decrypted, &key, &iv, block_size);
    let decrypted2 = crypto::cbc_decrypt(&reenc, &key, &iv, block_size);

    println!("whattt {:?}", buffer::stringify(&decrypted2));

}
