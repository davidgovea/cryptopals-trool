#![allow(dead_code)]
use std::str;

pub fn stringify(buffer: &Vec<u8>) -> &str {
    match str::from_utf8(buffer) {
        Ok(v) => v,
        Err(_) => "",
    }
}

pub fn xor_vec(text: &Vec<u8>, mut key: Vec<u8>) -> Vec<u8>{
    let mut result = Vec::new();
    for value in text.iter() {
        let key_byte = key.remove(0);
        key.push(key_byte);
        result.push(value ^ key_byte);
    }
    result
}

pub fn interleave(buffer: &Vec<u8>, bin_count: usize) -> Vec<Vec<u8>> {
    let mut interleaved: Vec<Vec<u8>> = Vec::new();
    for _ in 0..bin_count {
        // Prepare multi-dimensional vector
        interleaved.push(Vec::new());
    }

    let mut i = 0;
    for cipher_char in buffer.iter() {
        // Push character to appropriate interleaved vector
        interleaved[i].push(*cipher_char);

        i = i + 1;
        if i == bin_count {
            i = 0;
        }
    }
    interleaved
}

pub fn deinterleave(interleaved: Vec<Vec<u8>>) -> Vec<u8> {
    let mut deinterleaved: Vec<u8> = Vec::new();
    let bin_count = interleaved.len();
    let bin_length = interleaved[0].len();

    for step in 0..bin_length {
        for bin in 0..bin_count {
            deinterleaved.push(interleaved[bin][step]);
        }
    }
    deinterleaved
}
