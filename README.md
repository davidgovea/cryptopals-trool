# [cryptopals.com](https://cryptopals.com) challenges in Rust

## Running a challenge
After installing Rust, run a single challenge level with `cargo run --bin LEVEL`.

For example:  

```
$ cargo run --bin 1_1
$ cargo run --bin 2_11
```
